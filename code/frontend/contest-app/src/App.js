import { useState } from 'react';
import './App.css';
import Form from './Components/Form/Form';
import Welcome from './Components/WelcomePage/Welcome';
import ThankYou from './Components/ThankYouPage/ThankYou';
import ReadyToWinPage from './Components/ReadyToWinPage/ReadyToWinPage';
import ContestsPage from './Components/ContestsPage/ContestsPage';
import SurveyForm from './Components/surveyPage/SurveyForm';


function App() {


  const [stage, setStage] = useState(1);
  const [data, setData] = useState([]);

  const renderComponent = () => {
    switch (stage) {
      case 1:
        return <Welcome setStage={setStage} />;
      case 2:
        return <ReadyToWinPage setStage={setStage} data={data} setData={setData} />;
      case 3:
        return <ContestsPage setStage={setStage} data={data} setData={setData}/>;
      case 4:
        return <SurveyForm setStage={setStage} data={data} setData={setData}/>;
      case 5:
        return <Form setStage={setStage} data={data} setData={setData}/>;
      case 6:
        return <ThankYou />
      default:
        return <div>Invalid stage</div>; 
    }
  };

  return <div className="App" style={{height:'100vh'}}>{renderComponent()}</div>;
}

export default App;
