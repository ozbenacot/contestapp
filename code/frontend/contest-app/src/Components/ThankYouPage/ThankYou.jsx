import React, { useState, useEffect } from "react";
import "./thankYou.css";
import CircularIndeterminate from "../Progress/progress";

const ThankYou = () => {

  useEffect(() => {
    const timer = setTimeout(() => {
      window.location.replace("https://app.prolific.com/submissions/complete?cc=C3BT29M4"); // Replace with your target URL
    }, 4000);
  }, []);

  return (
    <div className="thankYouMainDiv">
      <div className="TYheaderDiv">Thank You For Your Participation!</div>

      <div className="TYcontentDiv">
        You are going to be directed to Prolific to complete the process...
      </div>
      <CircularIndeterminate />
    </div>
  );
};

export default ThankYou;
