import React from "react";
import { useState } from "react";
import "./Form.css";
import FormInput from "../FormInput/FormInput";
import ErrorDialog from "../Dialog/errorDialog";

const Form = (props) => {
  const [values, setValues] = useState({
    username: "X",
    prolificID: "",
    gender: "",
    country: "",
    age: "",
  });
  const [errors, setErrors] = useState({});
  const [openDialog, setOpenDialog] = useState(false);


  const handleClickOpen = () => {
    setOpenDialog(true);
  };

  const handleClose = () => {
    setOpenDialog(false);
  };

  const inputs = [
    {
      id: 2,
      name: "prolificID",
      type: "text",
      placeholder: "Prolific ID",
      label: "Prolific ID",
    },
    {
      id: 3,
      name: "gender",
      type: "select",
      options: ["Male", "Female", "Other", "Rather not say"],
      label: "Gender",
    },
    {
      id: 4,
      name: "age",
      type: "text",
      placeholder: "Age",
      label: "Age",
    },
    {
      id: 5,
      name: "country",
      type: "text",
      placeholder: "Country",
      label: "Country",
    },
  ];

  const validityCheck = () => {
    let newErrors = {};
    // if (values.username.trim() === "") {
    //   newErrors.username = true;
    // }
    if (values.prolificID.trim() === "") {
      newErrors.prolificID = true;
    }
    if (values.gender.trim() === "") {
      newErrors.gender = true;
    }
    if (values.country.trim() === "") {
      newErrors.country = true;
    }
    if (
      values.age.trim() === "" ||
      isNaN(values.age) ||
      values.age < 0 ||
      values.age > 120
    ) {
      newErrors.age = true;
    }

    setErrors(newErrors);

    if (Object.keys(newErrors).length > 0) {
      setOpenDialog(true);
      return false;
    }
  
    return true;
  };


  function getTimeDifference(contestStart, contestEnd) {
    // Calculate the difference in milliseconds
    const diffMilliseconds = contestEnd - contestStart;
  
    // Convert milliseconds to total seconds
    const totalSeconds = Math.floor(diffMilliseconds / 1000);
  
    // Calculate hours, minutes, and seconds
    const hours = Math.floor(totalSeconds / 3600);
    const minutes = Math.floor((totalSeconds % 3600) / 60);
    const seconds = totalSeconds % 60;
  
    // Format the result as HH:MM:SS
    const timeDifference = `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}:${String(seconds).padStart(2, '0')}`;
  
    return timeDifference;
  }


  const handleSubmit = async (e) => {
    e.preventDefault();
    const apiUrl = process.env.REACT_APP_API_URL;

    const contestStart = new Date(props.data[0].contestStart);
    const contestEnd = new Date(props.data[1].contestEnd);

    const contestsData = props.data[2].contests;
    const surveyData = props.data[3].surveyAnswers;
    
    const totalTimeSeconds = getTimeDifference(contestStart,contestEnd)
    
    const userData = {
      username: values.username,
      prolificID: values.prolificID,
      gender: values.gender,
      age: values.age,
      country: values.country,
    };

    const dataToSend = {
      userData,
      contestsData,
      surveyData,
      totalTimeSeconds,
    };
    // console.log(dataToSend);


   
    if (validityCheck()) {
      try {
        const response = await fetch(`${apiUrl}/api/user-submit`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(dataToSend),
        });
        if (response.ok) {
          const result = await response.json();
          props.setStage(6);
        } else {
          console.error("Error with submitting User data");
        }
      } catch (error) {
        console.error("Error:", error);
      }
    }
  };

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };
  
  // console.log(props.data);
  
  return (
    <div className="formMainDiv">
      <div className="FormHeaderDiv">Personal Details</div>
      <div className="PleaseContainer">Please enter your personal details:</div>
      <form onSubmit={handleSubmit} className="FormContainer">
        {inputs.map((input) => {
          if (input.type === "select") {
            return (
              <div key={input.id}>
                <div className="labelsContainer">
                  <label>{input.label}</label>
                </div>
                <div className="selectContainer">
                  <select
                    className={`selectInput ${
                      errors[input.name] ? "error" : ""
                    }`}
                    name={input.name}
                    value={values[input.name]}
                    onChange={onChange}
                  >
                    <option value="" disabled hidden>
                      -- Select --
                    </option>
                    {input.options.map((option) => (
                      <option key={option} value={option}>
                        {option}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            );
          } else {
            return (
              <FormInput
                key={input.id}
                error={`${errors[input.name] ? "error" : ""}`}
                {...input}
                value={values[input.name]}
                onChange={onChange}
              />
            );
          }
        })}

        <button className="submitBtn" type="submit">
          Submit
        </button>
      </form>
      <ErrorDialog OpenDialog={openDialog} handleClose={handleClose} errors={errors} />
      </div>
  );
};

export default Form;
