import React, { useEffect, useState } from "react";
import "./clock.css";
import PopUpDialog from "../Dialog/dialog";


const Clock = ({ setStage, handelLeave }) => {
  const TimeInMinutes = 5;
  const initialTime = TimeInMinutes * 60;
  const [timeLeft, setTimeLeft] = useState(initialTime);
  const [openDialog, setOpenDialog] = useState(false);

  const handleClickOpen = () => {
    console.log("test");
    setOpenDialog(true);
  };

  const handleClose = () => {
    setOpenDialog(false);
  };

  useEffect(() => {
    const timer = setInterval(() => {
      setTimeLeft((prevTime) => {
        if (prevTime <= 0) {
          clearInterval(timer);
          handleClickOpen();
          return 0;
        } else {
          return prevTime - 1;
        }
      });
    }, 1000);
    return () => clearInterval(timer);
  }, [setStage]);

  const minutesCalc = Math.floor(timeLeft / 60);
  const secondsCalc = timeLeft % 60;

  return (
    <div className="clockNPop">
      <div className="clockContainer">
        <p className="clockText">{`Time Left : ${minutesCalc
          .toString()
          .padStart(2, "0")}:${secondsCalc.toString().padStart(2, "0")}`}</p>
      </div>
      <div>
       <PopUpDialog OpenDialog={openDialog} handleClickOpen={handleClickOpen} handleClose={handleClose}  setStage={setStage} handelLeave={handelLeave}/>
      </div>
    </div>
  );
};

export default Clock;
