import React from "react";
import "./surveyInput.css";

const SurveyInput = (props) => {
  const { label, onChange, id, ...inputProps } = props;
  return (
    <div className="surveyInput">
      <div className="labelsContainer">
        <label>{label}:</label>
      </div>
      <div className="inputsContainer">
        <input type="checkbox" {...inputProps} onChange={onChange} />
      </div>
    </div>
  );
};

export default SurveyInput;
