import React, { useState } from "react";
import "./welcome.css";

const Welcome = (props) => {
  const [confirmed, setConfirmed] = useState(false);

  return (
    <div className="WelcomMainDiv">
      <div className="WelcomheaderDiv">Welcome</div>
      <div className="WcontentDiv">
        <p>Informed consent to participate in a study:</p>
        <ol>
          <li>You are about to participate in a decision-making study.</li>
          <li>
            During the study, you will be offered the opportunity to participate
            in naming contests for 5 minutes, after which you will be asked a
            few questions about your experience.
          </li>
          <li>
            You can withdraw your participation at any time or refuse to answer
            any question.
          </li>
          <li>
            Upon completion of the study, you will reach a completion page in
            which you will receive a verification code. This code will verify
            that you completed the study in Prolific so that you will receive a
            credit of $1.5 for your participation.
          </li>
          <li>
            Confidentiality as to the identity of each participant is
            guaranteed, and only summary data will be published. It is
            impossible to connect the personal details of the participant with
            the answers and data provided during the study.
          </li>
          <li>It should take you about 7 minutes to complete the study.</li>
          <li>
            The study has been approved by Ben-Gurion University’s ethics
            committee.
          </li>
          <li>
            If you have any questions about the study, please email Nitay Man
            at: mannit@post.bgu.ac.il
          </li>
          <li>
            If you are willing to participate in the study, please check the
            following box:
          </li>
        </ol>
        <input
          type="checkbox"
          onChange={() => setConfirmed(!confirmed)}
          id="confirm"
          name="confirm"
          checked={confirmed}
          className="confirmCheckbox" 
        />
        <label htmlFor="confirm">
          {" "}
          I hereby confirm that I have understood the above and freely give my
          consent to participate in this study
        </label>
      </div>
      <div>
        <button
          onClick={() => props.setStage(2)}
          disabled={!confirmed}
          className={`approveBtn ${confirmed ? "enabled" : "disabled"}`}
        >
          Continue
        </button>
      </div>
    </div>
  );
};

export default Welcome;
