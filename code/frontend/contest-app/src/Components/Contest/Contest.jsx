import React, { useEffect, useState } from "react";
import "./Contest.css";
import Close from "../../assets/hide.png";
import Open from "../../assets/view.png" ;

const Contest = (props) => {
  const [entityValue, setEntityValue] = useState("");
  const [localEntities, setLocalEntities] = useState(props.ContestUserEntities);

  useEffect(() => {
    setLocalEntities(props.ContestUserEntities);
  }, [props.ContestUserEntities]);

  const handleEntityClick = (index) => {
    const removedEntity = localEntities[index];
    const newEntities = localEntities.filter((_, i) => i !== index);
    setLocalEntities(newEntities);
    props.removeEntityFromContest(removedEntity, props.ContestName);
    setEntityValue(removedEntity.entity);
  };


  function timeStringToSeconds(timeString) {
    const [hours, minutes, seconds] = timeString.split(':').map(Number);
    return hours * 3600 + minutes * 60 + seconds;
}

  function secondsToTimeString(seconds) {
    const hours = Math.floor(seconds / 3600).toString().padStart(2, '0');
    const minutes = Math.floor((seconds % 3600) / 60).toString().padStart(2, '0');
    const remainingSeconds = (seconds % 60).toString().padStart(2, '0');
    return `${hours}:${minutes}:${remainingSeconds}`;
  }

  const culcTotalTime = (startTime, endTime) => {
    const start = new Date(startTime);
    const end = new Date(endTime);
    const totalTime = new Date(end - start);
    return totalTime.toISOString().substr(11, 8); // Format as HH:MM:SS
  }

  const updateTotalTime = (contestName, newTotalTime) => {
    props.setContestsList((prevList) =>
      prevList.map((contest) =>
        contest.ContestName === contestName
          ? { ...contest, totalTime: newTotalTime }
          : contest
      )
    );
  };

  const handleGoBack = () => {
    const totalTimeNow = culcTotalTime(props.selectedContest.StartTime, new Date());
    let currentTotalTime;
    currentTotalTime = props.selectedContest.totalTime

    const totalTimeInSeconds = timeStringToSeconds(totalTimeNow);
    const currentTotalTimeInSeconds = timeStringToSeconds(currentTotalTime);

    const sumOfTimesInSeconds = totalTimeInSeconds + currentTotalTimeInSeconds;
    const summedTime = secondsToTimeString(sumOfTimesInSeconds);

    // console.log(totalTimeNow);
    // console.log(currentTotalTime);
    // console.log(summedTime);
    updateTotalTime(props.selectedContest.ContestName, summedTime)
    props.setIsInContest(false);
    props.handelLeave();
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (entityValue.trim() === "") {
      console.log("Cannot submit an empty entity.");
      return;
    }

    const isDuplicate = localEntities.some(
      (entity) => entity.entity.trim() === entityValue.trim()
    );

    if (!isDuplicate) {
      const newEntity = { entity: entityValue.trim() };

      setLocalEntities((current) => [...current, newEntity]);

      props.addEntityToContest(newEntity, props.ContestName);
    } else {
      console.log("This entity already exists.");
    }

    setEntityValue("");
  };

  const openClose = props.IsOpenContest ? <img src={Open} alt="Open Contest" width="40px"/> : <img src={Close} alt="Closed Contest" width="30px"/>;

  return (
    <div className="contestMainDiv">
      <div className="headerDiv">{props.ContestName}</div>
      <div className="contentDiv">
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            marginTop: "25px",
          }}
        >
          <div className="contestDescriptionDiv">
            <div className="prizeBtn">{openClose}  |  {props.ContestPrize}  |  $30</div>
            <div>{props.ContestDescription}</div>
          </div>
        </div>

        {props.IsOpenContest && (
          <div className="suggestedEntities">
            <div>Expert Suggestions:</div>
            {props.suggestedEntities.map((entity, index) => (
              <div className="Sugentity" key={index}>
                {entity}
              </div>
            ))}
          </div>
        )}

        <div className="entitySubmitDiv">
          <form className="formContainer" onSubmit={handleSubmit}>
            <label>Your suggestion:</label>
            <input
              className="entityInput"
              name="entity"
              value={entityValue}
              onChange={(e) => setEntityValue(e.target.value)}
            />
            <button className="entityInputBtn" type="submit">
              Submit
            </button>
          </form>
        </div>
        <div className="userEntitys">
          {localEntities &&
            localEntities.map((entry, index) => (
              <div
                className="entity"
                key={index}
                onClick={() => handleEntityClick(index)}
              >
                {entry.entity}
              </div>
            ))}
        </div>
      </div>
      <div>
        <button onClick={handleGoBack}>
          Finish, go to final questions
        </button>
      </div>
    </div>
  );
};

export default Contest;
