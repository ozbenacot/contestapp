import React from "react";
import "./ContestsPage.css";
import Close from "../../assets/hide.png";
import Open from "../../assets/view.png" ;


const ContestBar = (props) => {


  const handleContestClick = () => {
    props.selectedContest.StartTime = new Date();
    props.setIsInContest(true);
    if (props.order < 6) {
      if (!props.contestsOrder.includes(props.selectedContest.ContestName)) {
        props.setContestsOrder([...props.contestsOrder, props.selectedContest.ContestName]);
        props.setOrder(props.order + 1);
      }
    }
    // console.log(props.contestsOrder);
  };

  return (
    <div className="ContestBar">
      <div className="btnDiv">
        <button className="contestBtn" onClick={handleContestClick}>
          {props.ContestName}
        </button>
      </div>
      <div className="detailsDiv">
      
        <div className="detailItem">{props.ContestPrize}</div> {/*ContestPrize = Opponent */}
        <div className="detailItem">{props.IsOpenContest ? <img src={Open} alt="Open Contest" width="50px"/> : <img src={Close} alt="Closed Contest" width="50px"/>}</div>
        <div className="detailItem">$30</div> 
      </div>
    </div>
  );
};

export default ContestBar;
