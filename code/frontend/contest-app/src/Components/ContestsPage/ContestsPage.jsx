import React, { useState, useEffect } from "react";
import "./ContestsPage.css";
import Clock from "../Clock/Clock";
import Contest from "../Contest/Contest";
import ContestBar from "./ContestBar";

const ContestsPage = (props) => {
  const [isInContest, setIsInContest] = useState(false);
  const [selectedContest, setSelectedContest] = useState(null);
  const [shuffledContests, setShuffledContests] = useState([]);
  const [contestsOrder, setContestsOrder] = useState([]);
  const [order, setOrder] = useState(0);

  // const [shuffledDetailsContests, setShuffledDetailsContests] = useState([]);

  const [ContestsList, setContestsList] = useState([
    {
      ContestName: "Name for an Adventure Travel Agency",
      ContestDescription:
        "Come up with an adventurous and inspiring name for a travel agency that specializes in off-the-beaten-path experiences and adventure travel. The name should evoke a sense of exploration and discovery.",
      suggestedEntities: [
        "Explorer's Edge",
        "Adventure Awaits Travel",
        "Pathfinders Journeys",
        "Wild Routes",
        "Beyond Boundaries",
        "Epic Escapes",
        "Trailblazer Travels",
        "Adventure Horizon",
        "Quest Quarters",
        "Discovery Treks",
      ],
      ContestUserEntities: [],
      StartTime: "00:00:00",
      LastTimeEnteredAt: null,
      totalTime: "00:00:00",
    },
    {
      ContestName: "Name for a Children's Educational App",
      ContestDescription:
        "Create a fun and engaging name for a new educational app designed for children. The app combines learning with play to make education enjoyable and interactive.",
      suggestedEntities: [
        "Brainy Bunch",
        "Smarty Sprouts",
        "FunLearn Kids",
        "EduPlay Lab",
        "Genius Gems",
        "Kiddy Academy",
        "Learn & Leap",
        "MindMovers",
        "Bright Minds",
        "WhizKid Workshop",
      ],
      ContestUserEntities: [],
      StartTime: "00:00:00",
      LastTimeEnteredAt: null,
      totalTime: "00:00:00",
    },
    {
      ContestName: "Name for a Vegan Restaurant Chain",
      ContestDescription:
        "Craft a unique and memorable name for a vegan restaurant chain that prides itself on delicious, plant-based meals. The name should evoke a sense of health, sustainability, and community.",
      suggestedEntities: [
        "Green Bites",
        "Veggie Vista",
        "Plant Plate",
        "Purely Vegan",
        "Vegan Vibe Eatery",
        "Eco Eats",
        "Harvest Hub",
        "Nature's Table",
        "Vegan Valley",
        "Fresh Fork",
      ],
      ContestUserEntities: [],
      StartTime: "00:00:00",
      LastTimeEnteredAt: null,
      totalTime: "00:00:00",
    },
    {
      ContestName: "Name for a Home Decor Brand Website",
      ContestDescription:
        "Come up with a stylish and memorable name for a home decor brand website. The name should reflect elegance, innovation, and the comfort of home.",
      suggestedEntities: [
        "ModernCraft Homes",
        "Decor Dreamscapes",
        "Innovate Decor",
        "Design Harmony",
        "Elegant Abode",
        "Homestyle Haven",
        "Craft & Comfort",
        "Decor Dynamics",
        "Chic Spaces",
        "Tradition & Trend",
      ],
      ContestUserEntities: [],
      StartTime: "00:00:00",
      LastTimeEnteredAt: null,
      totalTime: "00:00:00",
    },
  ]);

  const handelLeave = () => {
    const contestsData = ContestsList.map((contest, index) => {
      const positionInOrder = contestsOrder.indexOf(contest.ContestName);

      return {
        name: contest.ContestName,
        entities: contest.ContestUserEntities,
        startTime:
          positionInOrder !== -1
            ? `00:0${positionInOrder + 1}:00` // Position + 1 in minutes
            : "00:00:00", // Default to "00:00:00" if not in contestsOrder
        LastTimeEnteredAt: contest.LastTimeEnteredAt,
        prize: contestDetails[index].prize,
        isOpen: contestDetails[index].isOpen,
        totalTime: contest.totalTime,
      };
    });
    // console.log(contestsData);

    props.setData([
      ...props.data,
      { contestEnd: new Date() },
      { contests: contestsData },
    ]);

    props.setStage(4);
  };

  // Prize = Opponent
  const [contestDetails, setContestDetails] = useState([
    { prize: "AI", isOpen: true },
    { prize: "AI", isOpen: false },
    { prize: "Human", isOpen: true },
    { prize: "Human", isOpen: false },
  ]);

  const handleSelectContest = (selectedContest) => {
    const index = shuffledContests.findIndex(
      (contest) => contest.ContestName === selectedContest.ContestName
    );

    const dynamicDetails = contestDetails[index];

    const updatedContest = {
      ...selectedContest,
      prize: dynamicDetails.prize,
      isOpen: dynamicDetails.isOpen,
    };

    updateContestTimes(updatedContest.ContestName);

    setIsInContest(true);
    setSelectedContest(updatedContest);
  };

  const addEntityToContest = (entity, contestName) => {
    const updatedContests = ContestsList.map((contest) => {
      if (contest.ContestName === contestName) {
        return {
          ...contest,
          ContestUserEntities: [...contest.ContestUserEntities, entity],
        };
      }
      return contest;
    });
    setContestsList(updatedContests);
  };

  const updateContestTimes = (contestName) => {
    const updatedContests = ContestsList.map((contest) => {
      if (contest.ContestName === contestName) {
        const now = new Date();
        return {
          ...contest,
          StartTime: contest.StartTime != null ? contest.StartTime : now,
          LastTimeEnteredAt: now,
        };
      }
      return contest;
    });
    setContestsList(updatedContests);
  };
  const removeEntityFromContest = (entityToRemove, contestName) => {
    const updatedContests = ContestsList.map((contest) => {
      if (contest.ContestName === contestName) {
        const updatedEntities = contest.ContestUserEntities.filter(
          (entity) => entity.entity !== entityToRemove.entity
        );
        return { ...contest, ContestUserEntities: updatedEntities };
      }
      return contest;
    });
    setContestsList(updatedContests);
  };

  useEffect(() => {
    const details = shuffleArray([...contestDetails]);
    setContestDetails(details);
    // setShuffledDetailsContests(details);
  }, []);

  useEffect(() => {
    const contest = shuffleArray([...ContestsList]);
    setContestsList(contest);
    setShuffledContests(contest);
  }, []);

  useEffect(() => {
    if (shuffledContests.length > 0) {
      setShuffledContests([...ContestsList]);
    }
  }, [ContestsList]);

  const shuffleArray = (array) => {
    let currentIndex = array.length,
      temporaryValue,
      randomIndex;

    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  };

  return (
    <div className="contestsMainDiv">
      <Clock setStage={props.setStage} handelLeave={handelLeave} />

      {isInContest && selectedContest ? (
        <Contest
          selectedContest={selectedContest}
          ContestName={selectedContest.ContestName}
          ContestDescription={selectedContest.ContestDescription}
          ContestPrize={`${selectedContest.prize}`}
          IsOpenContest={selectedContest.isOpen}
          ContestUserEntities={selectedContest.ContestUserEntities}
          suggestedEntities={selectedContest.suggestedEntities}
          setIsInContest={setIsInContest}
          addEntityToContest={addEntityToContest}
          removeEntityFromContest={removeEntityFromContest}
          setContestsList={setContestsList}
          ContestsList={ContestsList}
          handelLeave={handelLeave}
        />
      ) : (
        <div
          style={{
            width: "100%",
            alignItems: "center",
            justifyContent: "center",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <div>
            <div className="CPageheaderDiv">Our Contests</div>
            <div className="CPageSubheaderDiv">
              You can participate in{" "}
              <strong>
                <u> only one</u>
              </strong>{" "}
              of the four contests
            </div>
          </div>
          <div className="tableContainer">
            <div className="tableHeader">
              <div className="headerItem">Contests</div>
              <div className="headerItem">Experts</div>
              <div className="headerItem">Expert suggestions</div>
              <div className="headerItem">Prize</div>
            </div>
          </div>
          <div className="contestsContentDiv">
            {shuffledContests.map((contest, index) => (
              <ContestBar
                key={index}
                ContestName={contest.ContestName}
                selectedContest={contest}
                ContestPrize={contestDetails[index].prize}
                IsOpenContest={contestDetails[index].isOpen}
                setIsInContest={() => handleSelectContest(contest)}
                setContestsList={setContestsList}
                order={order}
                setOrder={setOrder}
                contestsOrder={contestsOrder}
                setContestsOrder={setContestsOrder}
              />
            ))}
          </div>
          <div>
            <button onClick={handelLeave}>Go to final questions</button>
          </div>
        </div>
      )}
    </div>
  );
};

export default ContestsPage;
