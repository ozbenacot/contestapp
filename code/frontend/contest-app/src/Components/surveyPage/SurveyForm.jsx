import React, { useState } from 'react';
import './surveyForm.css';

const SurveyForm = (props) => {
  const [values, setValues] = useState({
    reasons: [],
    mainInfluence: '',
    creativityLevel: '',
    generativeLevel: ''
  });
  const [errors, setErrors] = useState({});
  
  const reasonsOptions = [
    "I want to maximize my chances of winning contests.",
    "I prefer contests in which fewer people would participate.",
    "I want to behave differently from other people.",
    "I want to demonstrate my creativity.",
    "I want to show that I can be more creative than generative AI.",
    "I want to show that I can be more creative than a human marketing expert.",
    "I believe I am good at this type of task.",
    "I believe I am not good at this type of task.",
    "I want to win a prize without working too much.",
    "I believe I will enjoy engaging with this task.",
    "I like to compete with others.",
    "I hate losing.",
    "I enjoy winning.",
    "I prefer to see name suggestions by generative AI to help me develop new ideas.",
    "I prefer to see name suggestions by human marketing experts to help me develop new ideas.",
    "I prefer to come up with ideas of my own without being affected by existing ideas.",
    "I choose contests based on their descriptions.",
    "I believe human experts are more creative than generative AI.",
    "I believe generative AI is more creative than human experts."
  ];

  const mainInfluenceOptions = [
    "I want to maximize my chances of winning contests.",
    "I prefer contests in which fewer people would participate.",
    "I want to behave differently from other people.",
    "I want to demonstrate my creativity.",
    "I want to show that I can be more creative than generative AI.",
    "I want to show that I can be more creative than a human marketing expert.",
    "I believe I am good at this type of task.",
    "I believe I am not good at this type of task.",
    "I want to win a prize without working too much.",
    "I believe I will enjoy engaging with this task.",
    "I like to compete with others.",
    "I hate losing.",
    "I enjoy winning.",
    "I prefer to see name suggestions by generative AI to help me develop new ideas.",
    "I prefer to see name suggestions by human marketing experts to help me develop new ideas.",
    "I prefer to come up with ideas of my own without being affected by existing ideas.",
    "I choose contests based on their descriptions.",
    "I believe human experts are more creative than generative AI.",
    "I believe generative AI is more creative than human marketing experts."
  ];

  const creativityLevelOptions = [
    'Requires a very large amount of creativity',
    'Requires a large amount of creativity',
    'Requires an average amount of creativity',
    'Requires a low amount of creativity',
    'Does not require creativity at all'
  ];

  const generativeLevelOptions = [
    "Human experts are much more creative than generative AI.",
    "Human experts are more creative than generative AI.",
    "Human experts are about as creative as generative AI.",
    "Human experts are less creative than generative AI.",
    "Human experts are much less creative than generative AI.",
    "It depends on the specific task.",
    "I don't know."
  ]


  //GenerativeLevel
  const handleCheckboxChange = (option) => {
    setValues(prevValues => {
      const updatedReasons = prevValues.reasons.includes(option)
        ? prevValues.reasons.filter(reason => reason !== option)
        : [...prevValues.reasons, option];
      return { ...prevValues, reasons: updatedReasons };
    });
  };

  const handleRadioChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    props.setData([
      ...props.data,
      {surveyAnswers: [
      {reasons: values.reasons},
      {mainInfluence: values.mainInfluence},
      {creativityLevel: values.creativityLevel},
      {generativeLevel: values.generativeLevel} ]},
    ])
    props.setStage(5);
    
  };

  return (
    <div className="surveyFormMainDiv">
      <div className="surveyHeaderDiv">Survey</div>
      <form className="surveyForm" onSubmit={handleSubmit}>
        <div className="questionContainer">
          <div className="questionHeader">From the following list of reasons, please choose <strong>all</strong> the reasons that explain your decision in which contest to participate:</div>
          {reasonsOptions.map(option => (
            <label key={option} className="checkboxContainer">
              <input
                className='selectionObject'
                type="checkbox"
                name="reasons"
                value={option}
                checked={values.reasons.includes(option)}
                onChange={() => handleCheckboxChange(option)}
              />
              {option}
            </label>
          ))}
          {errors.reasons && <div className="error">Please select at least one reason.</div>}
        </div>

        <div className="questionContainer">
          <div className="questionHeader">From the reasons you chose above, please select the sentence that best describes the main reason:</div>
          {mainInfluenceOptions.map(option => (
            <label key={option} className="radioContainer">
              <input
                className='selectionObject'
                type="radio"
                name="mainInfluence"
                value={option}
                checked={values.mainInfluence === option}
                onChange={handleRadioChange}
              />
              {option}
            </label>
          ))}
          {errors.mainInfluence && <div className="error">Please select an influence.</div>}
        </div>

        <div className="questionContainer">
          <div className="questionHeader">From the list below, please select the statement that best describes the level of creativity required in your daily life:</div>
          {creativityLevelOptions.map(option => (
            <label key={option} className="radioContainer">
              <input
                className='selectionObject'
                type="radio"
                name="creativityLevel"
                value={option}
                checked={values.creativityLevel === option}
                onChange={handleRadioChange}
              />
              {option}
            </label>
          ))}
          {errors.creativityLevel && <div className="error">Please select a creativity level.</div>}
        </div>

        <div className="questionContainer">
          <div className="questionHeader">From the following statements, please choose the statement you agree with the most:</div>
          {generativeLevelOptions.map(option => (
            <label key={option} className="radioContainer">
              <input
                className='selectionObject'
                type="radio"
                name="generativeLevel"
                value={option}
                checked={values.generativeLevel === option}
                onChange={handleRadioChange}
              />
              {option}
            </label>
          ))}
          {errors.generativeLevel && <div className="error">Please select an AI creativity level.</div>}
        </div>

        <button type="submit">
          Continue
        </button>
      </form>
    </div>
  );
};

export default SurveyForm;
