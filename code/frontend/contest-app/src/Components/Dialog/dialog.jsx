import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

export default function PopUpDialog(props) {
  const handleTimeEnd = () => {
    props.handleClose();
    props.handelLeave();
    props.setStage(4);
  };

  return (
    <React.Fragment>
      <Dialog
        open={props.OpenDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Time's Up!"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Your five minutes are over. Thank you for participating in the
            contests! We appreciate your time and effort. Please fill out the
            survey to provide your feedback.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleTimeEnd} autoFocus>
            Proceed to Survey
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
