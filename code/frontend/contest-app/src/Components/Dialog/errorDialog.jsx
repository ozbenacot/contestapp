import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

export default function ErrorDialog(props) {
  const getErrorMessages = () => {
    const errorMessages = [];
    if (props.errors.prolificID) {
      errorMessages.push("Prolific ID is required.");
    }
    if (props.errors.gender) {
      errorMessages.push("Gender is required.");
    }
    if (props.errors.country) {
      errorMessages.push("Country is required.");
    }
    if (props.errors.age) {
      errorMessages.push("Age must be a number between 0 and 120.");
    }
    return errorMessages;
  };

  return (
    <React.Fragment>
      <Dialog
        open={props.OpenDialog}
        onClose={props.handleClose}
        aria-labelledby="error-dialog-title"
        aria-describedby="error-dialog-description"
      >
        <DialogTitle id="error-dialog-title">{"Form Validation Errors"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="error-dialog-description">
            {getErrorMessages().map((msg, index) => (
              <div key={index}>{msg}</div>
            ))}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={props.handleClose} autoFocus>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
