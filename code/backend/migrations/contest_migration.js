'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable('Contests', {
      session_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'user_id'
        }
      },
      total_time: {
        type: Sequelize.TIME
      },
      contest_1_name: Sequelize.STRING,
      contest_1_prize: Sequelize.STRING,
      contest_1_isOpen: Sequelize.BOOLEAN,
      contest_1_total_time: Sequelize.TIME,
      contest_1_start_time: Sequelize.TIME,
      
      contest_2_name: Sequelize.STRING,
      contest_2_prize: Sequelize.STRING,
      contest_2_isOpen: Sequelize.BOOLEAN,
      contest_2_total_time: Sequelize.TIME,
      contest_2_start_time: Sequelize.TIME,
      
      contest_3_name: Sequelize.STRING,
      contest_3_prize: Sequelize.STRING,
      contest_3_isOpen: Sequelize.BOOLEAN,
      contest_3_total_time: Sequelize.TIME,
      contest_3_start_time: Sequelize.TIME,

      contest_4_name: Sequelize.STRING,
      contest_4_prize: Sequelize.STRING,
      contest_4_isOpen: Sequelize.BOOLEAN,
      contest_4_total_time: Sequelize.TIME,
      contest_4_start_time: Sequelize.TIME,

      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('now')
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('now')
      }
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable('Contests');
  }
};
