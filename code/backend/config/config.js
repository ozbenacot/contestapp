require('dotenv').config({path:"./code/backend/.env"});

console.log('DATABASE_URL:', process.env.DATABASE_URL); // Debugging

module.exports = {
  production: {
    use_env_variable: 'DATABASE_URL',
    dialect: 'postgres',
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
  },
};
