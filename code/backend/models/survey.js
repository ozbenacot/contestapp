module.exports = (sequelize, DataTypes) => {
    const Survey = sequelize.define('Survey', {
        session_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'User',
                key: 'user_id'
            }
        },
        q_explain_participate: {
            type: DataTypes.ARRAY(DataTypes.STRING),
            allowNull: true
        },
        q_influence_participate: {
            type: DataTypes.STRING,
            allowNull: true
        },  
        q_creativity: {
            type: DataTypes.STRING,
            allowNull: true
        },
        q_generativeAi: {
            type: DataTypes.STRING,
            allowNull: true
        },
    });

    return Survey;
};
