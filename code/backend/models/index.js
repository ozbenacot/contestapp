require('dotenv').config();
console.log(process.env.DATABASE_URL);  // Should print the database URL

import Sequelize from 'sequelize';
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
let sequelize;
 

console.log(process.env.DATABASE_URL);  // Should print the database URL

if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;


db.users = require('./user')(sequelize, Sequelize);
db.contests = require('./contest')(sequelize, Sequelize);
db.suggestions = require('./suggestion')(sequelize, Sequelize);
db.survey = require('./survey')(sequelize, Sequelize);

export default db;