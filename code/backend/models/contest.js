module.exports = (sequelize, DataTypes) => {
    const Contest = sequelize.define('Contest', {
        session_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'User',
                key: 'user_id'
            }
        },
        total_time: {
            type: DataTypes.TIME
        },

        // Contest 1
        contest_1_name: { 
            type: DataTypes.STRING ,
            allowNull: false
        },
        contest_1_prize: {
            type: DataTypes.STRING,
            allowNull: false
        },
        contest_1_isOpen: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        contest_1_total_time: {
            type: DataTypes.TIME,
            allowNull: true
        },
        contest_1_start_time: {
            type: DataTypes.TIME,
            allowNull: true
        },

        // Contest 2
        contest_2_name: { 
            type: DataTypes.STRING ,
            allowNull: false
        },
        contest_2_prize: {
            type: DataTypes.STRING,
            allowNull: false
        },
        contest_2_isOpen: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        contest_2_total_time: {
            type: DataTypes.TIME,
            allowNull: true
        },
        contest_2_start_time: {
            type: DataTypes.TIME,
            allowNull: true
        },

        // Contest 3
        contest_3_name: { 
            type: DataTypes.STRING ,
            allowNull: false
        },
        contest_3_prize: {
            type: DataTypes.STRING,
            allowNull: false
        },
        contest_3_isOpen: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        contest_3_total_time: {
            type: DataTypes.TIME,
            allowNull: true
        },
        contest_3_start_time: {
            type: DataTypes.TIME,
            allowNull: true
        },

        // Contest 4
        contest_4_name: { 
            type: DataTypes.STRING ,
            allowNull: false
        },
        contest_4_prize: {
            type: DataTypes.STRING,
            allowNull: false
        },
        contest_4_isOpen: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        contest_4_total_time: {
            type: DataTypes.TIME,
            allowNull: true
        },
        contest_4_start_time: {
            type: DataTypes.TIME,
            allowNull: true
        },
    });

    return Contest;
};
