module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        user_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        username: {
            type: DataTypes.STRING,
            allowNull: false
        },
        prolificID: {
            type: DataTypes.STRING,
            allowNull: false
        },
        gender: {
            type: DataTypes.ENUM,
            values: ['Male', 'Female', 'Rather not say', 'Other'],
            allowNull: false
        },
        age: {
            type: DataTypes.INTEGER
        },
        country: {
            type: DataTypes.STRING
        }
    });

    return User;
};
