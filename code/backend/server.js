const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const { Sequelize } = require('sequelize');
const contest = require('./models/contest');
const { time } = require('console');
require('dotenv').config(); // Load environment variables from .env file


// Initialize Sequelize with Heroku database URL
const sequelize = new Sequelize(process.env.DATABASE_URL, {
  dialect: 'postgres',
  protocol: 'postgres',
  dialectOptions: {
    ssl: {
      require: true,
      rejectUnauthorized: false // This is required for Heroku Postgres
    }
  },
  logging: false, // Disable logging if you don't want to see SQL queries in the console
});


// Import and initialize models
const User = require('./models/user')(sequelize, Sequelize.DataTypes);
const Contest = require('./models/contest')(sequelize, Sequelize.DataTypes);
const Suggestion = require('./models/suggestion')(sequelize, Sequelize.DataTypes);
const Survey = require('./models/survey')(sequelize, Sequelize.DataTypes);


// Test the database connection
sequelize.authenticate().then(() => {
  console.log('Connection has been established successfully.');
}).catch(err => {
  console.error('Unable to connect to the database:', err);
});

const app = express();

// Middleware
app.use(bodyParser.json());
app.use(cors());

// Serve static files from the React app
app.use(express.static(path.join(__dirname, '../../code/frontend/contest-app/build')));

// Routes
app.get('/api', (req, res) => {
  res.send('Server is up and running');
});

app.get('/api/users', async (req, res) => {
  try {
    const users = await User.findAll();
    res.json(users);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

app.get('/api/test-get', async (req, res) => {
  try {
    const examples = await Example.findAll();
    res.json(examples);
  } catch (error) {
    res.status(500).json({ error: 'Failed to fetch data' });
  }
});

// Endpoint to add data to the database
app.post('/api/user-submit', async (req, res) => {
  const { userData, contestsData, surveyData, totalTimeSeconds } = req.body;

  // console.log(contestsData);
  
  try {
    // Create a new user
    const newUser = await User.create({
      username: userData.username,
      prolificID: userData.prolificID,
      gender: userData.gender,
      age: userData.age,
      country: userData.country,
    });
  
    const userContests = await Contest.create({
      user_id: newUser.user_id,
      total_time: totalTimeSeconds,
      contest_1_name: contestsData[0].name,
      contest_1_prize: contestsData[0].prize,
      contest_1_isOpen: contestsData[0].isOpen,
      contest_1_total_time: contestsData[0].totalTime,
      contest_1_start_time: contestsData[0].startTime,
      
      contest_2_name: contestsData[1].name,
      contest_2_prize: contestsData[1].prize,
      contest_2_isOpen: contestsData[1].isOpen,
      contest_2_total_time: contestsData[1].totalTime,
      contest_2_start_time: contestsData[1].startTime,
            
      contest_3_name: contestsData[2].name,
      contest_3_prize: contestsData[2].prize,
      contest_3_isOpen: contestsData[2].isOpen,
      contest_3_total_time: contestsData[2].totalTime,
      contest_3_start_time: contestsData[2].startTime,

      contest_4_name: contestsData[3].name,
      contest_4_prize: contestsData[3].prize,
      contest_4_isOpen: contestsData[3].isOpen,
      contest_4_total_time: contestsData[3].totalTime,
      contest_4_start_time: contestsData[3].startTime,
    })
    
    contestsData[0].entities.forEach(entity => {
      Suggestion.create({
        user_id: newUser.user_id,
        contest_name: contestsData[0].name,
        entity: entity.entity,
        time: '00:00:00',
      });      
    });

    contestsData[1].entities.forEach(entity => {
      Suggestion.create({
        user_id: newUser.user_id,
        contest_name: contestsData[1].name,
        entity: entity.entity,
        time: '00:00:00',
      });      
    });

    contestsData[2].entities.forEach(entity => {
      Suggestion.create({
        user_id: newUser.user_id,
        contest_name: contestsData[2].name,
        entity: entity.entity,
        time: '00:00:00',
      });      
    });

    contestsData[3].entities.forEach(entity => {
      Suggestion.create({
        user_id: newUser.user_id,
        contest_name: contestsData[3].name,
        entity: entity.entity,
        time: '00:00:00',
      });      
    })
    
    // Create survey answers related to this user
    const newSurvey = await Survey.create({
      user_id: newUser.user_id,
      q_explain_participate: surveyData[0].reasons,
      q_influence_participate: surveyData[1].mainInfluence,
      q_creativity: surveyData[2].creativityLevel,
      q_generativeAi: surveyData[3].generativeLevel,
    });
    
    res.status(201).json({
      user: newUser,
      contest: userContests,
      survey: newSurvey,
      contests: 'User, Contests and suggestions and Survey created successfully',
    });
  } catch (error) {
    console.error('Error creating user and related data:', error);
    res.status(500).json({ error: 'Failed to create user and related data' });
  }
});

// The "catchall" handler: for any request that doesn't match one above, send back React's index.html file.
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../../code/frontend/contest-app/build/index.html'));
});

// Start server
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
